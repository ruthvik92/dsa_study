
# coding: utf-8

# A general linkedlist class
# 
# I am writing my own solutions and some extra methods

# In[1]:


class Node:   ##just to call it parent node
    def __init__(self,val):
        self.val = val
        self.next = None # the pointer initially points to nothing


# in the previous code we just created a node class

# In[2]:


class Linkedlist(Node):
    def __init__(self,val):  #                                                   * * * * *
        super(Linkedlist, self).__init__(val) ##just to practice OOP with python (- _ -)
        
 ##if your head doesn't spin yet see this https://stackoverflow.com/questions/576169/
#                                       understanding-python-super-with-init-methods
        
    #def __init__(self,val):
    #    self.val = val
    #    self.next = None # "regular" way
        self.size=None
    def traverse(self):
        node = self #start at the present object
        while node.next!=None: #this while loop loop will not
            print(node.val)    #print the contents of the last node
            node=node.next 
        if(node.next==None): #this will print the contents of the
            print(node.val)  #last node.

    def insert_end(self,next_node): ##traverse till end and point the
        node =self                  ## last node to the nex node that you
        while node.next!=None:      ## want to insert at the end.
            node=node.next
        node.next = next_node
    
    def insert_before(self,prev_node): ##takes the start node and prev_node
        node =self                  ## points to the start node.
        prev_node.next = node       ## insert a node before head node(but why you wanna do this??)
    
    def insert_begin(self,new_node):  ##insert a node right infront of the head node.
        temp = self.next
        self.next = new_node
        new_node.next = temp
    
    def insert_nth_pos(self,new_node,position): ##insert a node in nth place.
        if(position==1):  ##this case requires special case becuse in the for loop below, if position=1
            prev_first = self.next ## it won't loop even ones also.
            self.next = new_node
            new_node.next=prev_first
            return 
        node = self.next        ##create the variable node to traverse the LL, this will make one step.
        for i in range(1,position-1): ##this loop will take you till the nth node in the LL.
            node = node.next 
        temp = node.next           ##see where the nth node is pointing to, to make the link with new node.
        node.next = new_node   ##create link between new node and nth node.
        new_node.next =temp    ##create link between new node and rest of the LL.
    
    def delete_nth_node(self,position): ##delete nth node
        node = self.next
        if(position==1):
            retval = node
            temp = node.next
            self.next = temp
            return retval
        
        for i in range(1,position-1):
            node = node.next
        retval = a.next
        node.next = node.next.next
        return retval
            
    def remove_duplicates(self):  ##remove any duplicates
        vals = []
        node = self.next #go to the starting node.
        while node!=None:   ##don't go further if you are in none node.
            if(node.val in vals): ##this will never be true for first iteration.
                 prev_node.next=node.next ##jump the nodes only when you find a duplicte.
            else:
                prev_node = node
                vals.append(node.val) ##start appending later, in the beginning vals should be empty.
            node=node.next
            
        return
        
    def fetch_nth_node(self,position): ## fetch nth node in the LL from he beginning.
        node = self.next
        for i in range(1,position):
            node = node.next
        return node.val
        
    def fetch_nth_last_node(self,position): ## fetch nth to the last node when he length is not known.
        #LL_len = 0                    ##this one first counts the length of LL and calls self.fetch_nth_node
        #node = self.next
        #while(node!=None):
        #    node = node.next
        #    LL_len+=1
        LL_len = self.sizeofll()
        return self.fetch_nth_node(LL_len-position+1)
    
    def reverse_iterate(self): ##reverse the LL using iteraion
        head = self             ##time ~ O(n), space ~ O(1)
        current = self.next
        next_ = None
        prev = None
        while(current!=None):
            next_ = current.next
            current.next = prev
            prev = current
            current = next_
            
        head.next= prev
        
        return head 
    
    def efficient_nth_last_fetch(self,position): ## fetch the nth to last node efficiently using 
        p1 = self.next                       ## runner's method or 2 pointer method.
        p2 = self.next
        for i in range(1,position):
            p2 = p2.next
        while(p2.next != None):
            p1 = p1.next
            p2 = p2.next
        return p1.val
    
    
    def RecurPrint(self,nextAddr):        ##print the conents of the LL using recursion.
        print(nextAddr.val)
        if(nextAddr.next==None):     ##base condtition for recursion.
            return
        self.RecurPrint(nextAddr.next)
        return
    
    def RevRecurPrint(self,nextAddr):
        if(nextAddr==None):     ##base condtition for recursion.
            return
        self.RevRecurPrint(nextAddr.next)
        print(nextAddr.val)
        return
    
    def reversion(self,nextAddr): #LL reversal using recursion. time ~ O(n), space ~ O(n) Implicit stack
        p = nextAddr
        if(p.next==None): #if you're at the end then point head towards the end.
            head.next=p
            return       
        self.reversion(p.next) #this recursion will call the next address.
        q = p.next#      {  All these are put in a stack(FILO)
        q.next = p#     {   do stack trace if you can't understand this method.
        p.next = None#  {
    
    def sizeofll(self):
        node = self
        self.size=0
        while(node.next!=None):
            node=node.next
            self.size+=1
        return self.size


# In[3]:


head = Linkedlist(val='head')
node1 = Linkedlist(val='a')
head.next = node1
head.traverse()
print()
node2 = Linkedlist(val='b')
head.insert_end(node2)
head.traverse()
print()
node4 = Linkedlist(val='d')
head.insert_begin(node4)
head.traverse()


# let's write a for loop to insert a node in front(next to) of the head node i.e node1.

# In[4]:


for j in range(3):
    head.insert_begin(Linkedlist(val=j))
    head.traverse()
    print()


# In[5]:


print('node next to head node is:{}'.format(head.delete_nth_node(1).val))
head.traverse()


# In[6]:


print('size of linkedlist:{}'.format(head.sizeofll()))


# Head node is 0th node and it will contain the tag, "head" and it points to the first node in the LL.

# In[7]:


head.insert_nth_pos(Node(val=10),1) #insert node in the first place.
head.insert_nth_pos(Node(val=20),2)
head.insert_nth_pos(Node(val=30),4)
head.insert_nth_pos(Node(val=40),5)
head.insert_end(Node(val='b'))
head.insert_end(Node(val='c'))
head.insert_end(Node(val='c'))
head.insert_end(Node(val=10))
head.traverse()
print()
print('LL with duplicates removed')
head.remove_duplicates()
head.traverse()
print('Feching nth node')
print(head.fetch_nth_node(1))
print('size of linkedlist:{}'.format(head.sizeofll()))


# In[8]:


print('Feching last nth node')
print(head.fetch_nth_last_node(10))
print(head.efficient_nth_last_fetch(10))
print()
print(head.fetch_nth_last_node(2))
print(head.efficient_nth_last_fetch(2))
print()
print(head.fetch_nth_last_node(3))
print(head.efficient_nth_last_fetch(3))


# In[9]:


head=head.reverse_iterate()
head.traverse()


# In[10]:


head.RecurPrint(head)


# In[11]:


head.RevRecurPrint(head)


# In[12]:


head.reversion(head.next)
head.traverse()


# In[13]:


head = Linkedlist(val='head')
for j in range(5):
    head.insert_begin(Node(val=j))
head.traverse()


# # Doubly linked list (TODO)

# In[14]:


class dubLinkedlist(Linkedlist):       ##inherit the Node class to use some of the methods from Node class, node class
    def __init__(self,val):  # will further inherit general pNode class which has a linkedlist implementation.
        super(dubLinkedlist, self).__init__(val)
        self.prev = None
    
    def InsertAtEnd(self,new): #insert node at the end
        new_node = new
        #if(self.next==None):
        #    self.next = new_node
        #    new_node.prev = self
        #else:
        node = self
        while node.next!=None:      ## want to insert at the end.
            node=node.next
        node.next = new_node
        new_node.prev = node
        return
            
    def InsertAtHead(self,new): ##insert right infront of the node.
        temp = self.next
        self.next = new
        new.prev=self
        new.next = temp
        temp.prev = new
        return 
    
    def revtraverse(self): ##print from the last node.
        node = self
        while node.next!=None: ##travel all the way back to the end.
            node=node.next
        
        while node!=None: ## print while on way back to head node.
            print(node.val)
            node=node.prev
        
        return
        


# In[15]:


head=dubLinkedlist(val='head')
print(head.prev)
print(head.next)


# In[16]:


head.InsertAtEnd(dubLinkedlist(val=1))
head.InsertAtEnd(dubLinkedlist(val=3))
head.InsertAtHead(dubLinkedlist(val=0))
head.InsertAtHead(dubLinkedlist(val='A'))
head.InsertAtHead(dubLinkedlist(val='C'))
head.traverse()
print()
head.revtraverse()

