def merge(a,b,A):
    i=0
    j=0
    k=0
    while(i<len(a) and j<len(b)): #merge 2 arrays, iterate over 2 arrays comparing
        #print                  #2 arrays and fill in(overwrite) the original array 
        if(a[i]<b[j]):          #witht he smallest from he comparison. If left or right
            A[k]=a[i]          #array is exhausted then fill the original array with
            i+=1              #remaining elements in the non empty array.
            k+=1
        else:
            A[k]=b[j]
            j+=1
            k+=1
    while(i<len(a)):
        A[k]=a[i]
        k+=1
        i+=1
    while(j<len(b)):
        A[k]=b[j]
        j+=1
        k+=1
    return A

#print merge(a,b)

def mergesort(A):
    n=len(A)
    if(n<2):
        return
    mid = n/2
    left = []
    right = []
    for i in range(0,mid):
        left.append(A[i])
    for i in range(mid,n):
        right.append(A[i])
    mergesort(left)        ##whenever there is a recursive call, all the variables along with 
    mergesort(right)        #next line to be executed are placed in a stack FILO(here, it stores 
    merge(left,right,A)      ## left and right arrays)

A = [4,5,6,-1,3,8,1]

mergesort(A)
