# dsa_study

data structures and algorithms practice and study.

If you are opening .ipynb files make sure that you select Ipython Notebook from the drop down menu near the EDIT option towards the right side of the web page.

I have been coding in python for about 4 years for image processing, computer vision, wireless/wired sensor networks and spiking neural network and  neural networks
in general, being an electrical and electronics engineer I was never trained rigorously in  data structures and algorithms but I did use a few algorithms like
quicksort, median of medians and a few others in various courses and research work at Boise State University. Many times I started learning data structures and
algorithms and dropped it because of the laguange complexities like pointers in c/c++ or having to learn a new language(Java) just to learn data structures,
being an electrical engineer I did do some c/c++ (for about 8 months each), I do understand poiters but I am not a pro to use them to learn another complex
subject( data strucures). My frustraiton finally pushed me to use Python to learn data structures, I consider my self an intermediate to advanced programmer
in Python at least in my fields.
 
I came across this tutorial from [here](https://medium.com/@kojinoshiba/data-structures-in-python-series-1-linked-lists-d9f848537b4d) on basic data structures like
linked lists, arrays, stacks, queues. Another good source is MyCodeSchool on youtube and Cracking the coding interview question book, I will try to solve(code)
as many problems as I can from this three sources. I will mention the source of the questions in comments inside the code.

I am using linkedlist.py as a package in stack.ipynb, apparently .ipynb files are not same as .py files so, in order to import linkedlist.ipynb to anywhere else,
firstly, you will have to save your linkedlist.ipynb as .py files and then import it elsewhere.
