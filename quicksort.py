import sys
#
A=[2,1,3,4]
##Hold the last element as a pivot and place a wall before the list,
##start moving from left to right. Whenever 
##there is an element that is smaller than the pivot sawp the
##smaller element with the element that is to the 
##right side of the wall and move the wall one position to the
##right, this wall movement is to ensure that all
##the elements that are smaller than pivot are to the left side
##of the wall and remaining to the right side. 
##Finally, swap the last element(pivot) with the element that is
##to right side of the wall(pIndex-th)

def Partition(A,start,end):
    pivot = A[end]        ##Start with the last element as pivot
    pIndex = start       ##Partition index or wall, start with the first element.
    for i in range(start,end): ##Whenever there is a present element smaller than pivot element exchange present 
        if(A[i]<=pivot):       #present element with A[pIndex] or wall and increase pIndex by 1. This increase is to
            c = A[i]           #move the wall to right.
            A[i]=A[pIndex]
            A[pIndex]=c
            pIndex+=1
    k = A[end]
    A[end]=A[pIndex]
    A[pIndex]=k
    #print(A)
    return pIndex

#Partition(A,0,2)
#print(A)

def Quicksort(A,start,end):
    if(start<end):
        #print('before:{}'.format(A))
        pIndex = Partition(A,start,end)
        #print('after:{}'.format(A))
        #print('partition index or wall: {}'.format(pIndex))
        #print(A,pIndex)
        Quicksort(A,start,pIndex-1)
        Quicksort(A,pIndex+1,end)
    else:
        return

#Quicksort(A,0,len(A)-1)
#print(A)
#In quicksort, we recursively sort both branches, leading to best-case O(n log n) time.
#However, when doing selection, we already know which partition our desired element lies in,
#since the pivot is in its final sorted position, with all those preceding it in an unsorted
#order and all those following it in an unsorted order. Therefore, a single recursive call locates
#the desired element in the correct partition, and we build upon this for quickselect:

# In other words, we recurse only in the section(left or right) where we expec to find the
#nth smallest element.
A =[-1,2,1,6,8,9,3,-3]
def Quickselect(A,start,end,k):
    if(start==end):
        return A[start]
    pivotIndex=Partition(A,start,end)
    if(k==pivotIndex):
        return A[k]
    elif(k<pivotIndex):
        return Quickselect(A,start,pivotIndex-1,k)
    else:
        return Quickselect(A,pivotIndex+1,end,k)
    
print(Quickselect(A,0,len(A)-1,int(len(A)/2)))
